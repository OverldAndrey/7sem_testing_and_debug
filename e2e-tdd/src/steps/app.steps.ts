import { Before, Given, When, Then } from 'cucumber';

import { expect } from 'chai';

import { AppPage } from '../app.po';
import {as} from 'pg-promise';

let page: AppPage;
let _worker: number;
let _correspondent: number;
let _project: number;
let _case: number;
let _description: string;
let _titleText: string;
let _tableText: string;

const wait = 1000;

Before(async () => {
    page = new AppPage();

    await page.navigateTo();
});

Given(/^I am worker (.*) received document from (.*) associated with (.*) from (.*) and described as (.*)$/,
    async (workerId: number, correspondentId: number, projectId: number, caseId: number, description: string) => {
    _worker = workerId;
    _correspondent = correspondentId;
    _project = projectId;
    _case = caseId;
    _description = description;
});

When(/I look at the app/, async () => {
    _titleText = await page.getTitleText();
});

Then(/I see the title and no dialogs opened/, async () => {
    expect(_titleText).to.contain('Система внутреннего документооборота');

    expect(await page.getAddDocumentDialogPresence()).to.be.false;
});

When(/I open add document dialog/, async () => {
    await page.getAddDocumentButton().click();
});

Then(/It should be opened/, async () => {
    expect(await page.getAddDocumentDialogPresence()).to.be.true;
});

When(/I enter the data and click add button/, {timeout: 100000}, async () => {
    const cors = page.getCorrespondentField();
    await cors.click();
    await page.sleep(wait);
    await page.getMatSelectWithValue(_correspondent).click();

    const wrks = page.getWorkerField();
    await wrks.click();
    await page.sleep(wait);
    await page.getMatSelectWithValue(_worker).click();

    const projs = page.getProjectField();
    await projs.click();
    await page.sleep(wait);
    await page.getMatSelectWithValue(_project).click();

    const cases = page.getCaseField();
    await cases.click();
    await page.sleep(wait);
    await page.getMatSelectWithValue(_case).click();

    await page.getDescriptionField().sendKeys(_description);

    await page.getCommitDocumentButton().click();

    await page.sleep(wait);
});

Then(/Dialog should close/, async () => {
    expect(await page.getAddDocumentDialogPresence()).to.be.false;

    await page.sleep(wait);
});

When(/I look at the table/, async () => {
    _tableText = await page.getDocumentTableText();
});

Then(/I should see my document appear and be able to display its details/, async () => {
    expect(_tableText).to.contain(_description);

    await page.getDocumentTableFirstRow().click();

    expect(await page.getDocumentDescriptionDialogPresence()).to.be.true;
    expect(await page.getDocumentDescriptionDialogTableText()).to.contain(_description);
});
