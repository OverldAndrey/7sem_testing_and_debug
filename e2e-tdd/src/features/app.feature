Feature: Open app
    Add document and see in displayed

    Scenario Outline: Adding document
        Given I am worker <workerId> received document from <correspondentId> associated with <projectId> from <caseId> and described as <description>

        When I look at the app
        Then I see the title and no dialogs opened

        When I open add document dialog
        Then It should be opened

        When I enter the data and click add button
        Then Dialog should close

        When I look at the table
        Then I should see my document appear and be able to display its details

        Examples:
            | workerId | correspondentId | projectId | caseId | description     |
            | 1        | 2               | 2         | 2      | First document  |
            | 2        | 1               | 1         | 1      | Second document |
            | 3        | 2               | 2         | 1      | Third document  |
            | 1        | 2               | 1         | 2      | Fourth document |
