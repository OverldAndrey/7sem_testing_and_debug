import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app/components/app/app.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MainComponent } from './interface/components/main/main.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SearchParamsComponent } from './interface/components/search-params/search-params.component';
import { DocumentTableComponent } from './interface/components/document-table/document-table.component';
import { AddDocumentCardComponent } from './interface/components/add-document-card/add-document-card.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialProxyModule } from './material-proxy/material-proxy.module';
import { DocumentDetailsComponent } from './interface/components/document-details/document-details.component';
import { HelpComponent } from './interface/components/help/help.component';
import { ApprovalDialogComponent } from './interface/components/approval-dialog/approval-dialog.component';
import { EditDocumentCardComponent } from './interface/components/edit-document-card/edit-document-card.component';
import { CaseService } from './data-access/services/case.service';
import { CorrespondentService } from './data-access/services/correspondent.service';
import { ProjectService } from './data-access/services/project.service';
import { WorkerService } from './data-access/services/worker.service';
import { DocumentService } from './data-access/services/document.service';


export function initializeServices(
    cases: CaseService,
    correspondents: CorrespondentService,
    projects: ProjectService,
    workers: WorkerService,
    documents: DocumentService
) {
    return async () => {
        try {
            await cases.init();
            await correspondents.init();
            await projects.init();
            await workers.init();
            await documents.init();
        } catch (e) {
            console.log(e);
        }
    };
}


@NgModule({
    declarations: [
        AppComponent,
        MainComponent,
        SearchParamsComponent,
        DocumentTableComponent,
        AddDocumentCardComponent,
        DocumentDetailsComponent,
        HelpComponent,
        ApprovalDialogComponent,
        EditDocumentCardComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MaterialProxyModule,
    ],
    providers: [
        {
            provide: APP_INITIALIZER,
            useFactory: initializeServices,
            multi: true,
            deps: [
                CaseService,
                CorrespondentService,
                ProjectService,
                WorkerService,
                DocumentService
            ]
        }
    ],
    entryComponents: [
        AddDocumentCardComponent,
        EditDocumentCardComponent,
        DocumentDetailsComponent,
        HelpComponent,
        ApprovalDialogComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
