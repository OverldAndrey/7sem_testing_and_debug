import { PDocument } from '../models/document';
import { Moment } from 'moment';
import * as faker from 'faker';
import { PFile } from '../models/file';
import { Correspondent } from '../models/correspondent';
import { Case } from '../models/case';
import { SearchParams } from '../models/search-params';
import * as moment from 'moment';

export class DataBuilder {
    static fakeDocument(date: Moment): PDocument {
        return {
            id: faker.random.number(100),
            description: faker.lorem.lines(3),
            case: faker.commerce.productAdjective(),
            createDate: date,
            stringId: faker.random.arrayElement(['Вх.ПС-', 'ПСТ-']) + faker.random.number({min: 100, max: 200}),
            parentStringId: '',
            correspondent: faker.commerce.department(),
            otherCorrespondent: '',
            project: faker.random.word(),
            worker: faker.name.jobTitle()
        } as PDocument;
    }

    static fakeDocuments(date: Moment, n: number): PDocument[] {
        const array = [];
        for (let i = 0; i < n; i++) { array.push(this.fakeDocument(date)); }
        return faker.random.arrayElements(array, array.length);
    }

    static fakeFile(docId?: number): PFile {
        return {
            id: faker.random.number(100),
            type: faker.random.word(),
            name: faker.random.word(),
            data: faker.image.cats(),
            document: docId || faker.random.number(100)
        } as PFile;
    }

    static fakeFiles(n: number, docId?: number): PFile[] {
        const array = [];
        for (let i = 0; i < n; i++) { array.push(this.fakeFile(docId)); }
        return faker.random.arrayElements(array, array.length);
    }

    static fakeProject() {
        return {
            groupName: faker.company.bsNoun(),
            isActive: faker.random.boolean(),
            projgroups: { name: faker.company.bsNoun() },
            id: faker.random.number(100)
        };
    }

    static fakeProjects(n: number) {
        const array = [];
        for (let i = 0; i < n; i++) { array.push(this.fakeProject()); }
        return faker.random.arrayElements(array, array.length);
    }

    static fakeWorker() {
        return {
            id: faker.random.number(100),
            email: faker.internet.email(),
            firstname: faker.name.firstName(),
            secondname: faker.name.lastName(),
            isActive: faker.random.boolean(),
            phone: faker.phone.phoneNumber(),
            positions: { name: faker.name.jobTitle() },
        };
    }

    static fakeWorkers(n: number) {
        const array = [];
        for (let i = 0; i < n; i++) { array.push(this.fakeWorker()); }
        return faker.random.arrayElements(array, array.length);
    }

    static fakeCorrespondent() {
        return {
            id: faker.random.number(100),
            email: faker.internet.email(),
            name: faker.company.companyName(),
            phone: faker.phone.phoneNumber()
        } as Correspondent;
    }

    static fakeCorrespondents(n: number) {
        const array = [];
        for (let i = 0; i < n; i++) { array.push(this.fakeCorrespondent()); }
        return faker.random.arrayElements(array, array.length);
    }

    static fakeCase() {
        return {
            id: faker.random.number(100),
            location: faker.address.zipCode(),
            name: faker.company.bsNoun()
        } as Case;
    }

    static fakeCases(n: number) {
        const array = [];
        for (let i = 0; i < n; i++) { array.push(this.fakeCase()); }
        return faker.random.arrayElements(array, array.length);
    }

    static fakeString() {
        return faker.lorem.word();
    }

    static fakeBlob() {
        return new Blob([faker.lorem.word()]);
    }

    static fakeNumber() {
        return faker.random.number();
    }
}

export class SearchParamsBuilder implements SearchParams {
    query = '';
    sort: 'createDateSort' | 'correspondentSort' | 'stringIdSort' = 'stringIdSort';
    sortOrder: 'asc' | 'desc' = 'asc';
    type: 'in' | 'out' | 'all' = 'all';
    createDateTo?: Moment;
    createDateFrom?: Moment;

    constructor() { }

    extract() {
        return {
            query: this.query,
            sort: this.sort,
            sortOrder: this.sortOrder,
            type: this.type,
            createDateTo: this.createDateTo,
            createDateFrom: this.createDateFrom
        } as SearchParams;
    }

    setQuery(query: string) {
        this.query = query;
        return this;
    }

    setSort(sort: 'createDateSort' | 'correspondentSort' | 'stringIdSort') {
        this.sort = sort;
        return this;
    }

    setSortOrder(sortOrder: 'asc' | 'desc') {
        this.sortOrder = sortOrder;
        return this;
    }

    setType(type: 'in' | 'out' | 'all') {
        this.type = type;
        return this;
    }

    setTimesToNow() {
        this.createDateFrom = moment(Date.now());
        this.createDateTo = moment(Date.now());
        return this;
    }

    setDefault() {
        this.query = '';
        this.sort = 'stringIdSort';
        this.sortOrder = 'asc';
        this.type = 'all';
        this.createDateTo = undefined;
        this.createDateFrom = undefined;
        return this;
    }
}
