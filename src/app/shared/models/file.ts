export interface PFile {
    id?: number,
    document: number,
    name: string,
    type: string,
    data: Blob | string
}
