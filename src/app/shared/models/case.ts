export interface Case {
    id: number,
    name: string,
    location: string
}
