import { Moment } from 'moment';

export interface SearchParams {
    query: string;
    type: 'in' | 'out' | 'all';
    createDateFrom?: Moment | undefined;
    createDateTo?: Moment | undefined;
    sortOrder: 'asc' | 'desc';
    sort: 'createDateSort' | 'correspondentSort' | 'stringIdSort';
}
