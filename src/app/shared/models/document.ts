import { Moment } from 'moment';

export interface PDocument {
    id: number;
    stringId: string;
    parentStringId: string;
    correspondent: string;
    otherCorrespondent: string;
    case: string;
    worker: string;
    project: string;
    description: string;
    createDate: Moment | string;
}

export const documentFieldNames = [
    'id',
    'stringId',
    'createDate',
    'parentStringId',
    'correspondent',
    'otherCorrespondent',
    'case',
    'worker',
    'project',
    'description',
];

export const documentFieldNamesRus = [
    '№',
    'Рег. номер',
    'Дата создания',
    'В ответ на',
    'Адресат',
    'Адресат (прочие)',
    'Хранилище',
    'Исполнитель',
    'Тематика',
    'Описание',
];
