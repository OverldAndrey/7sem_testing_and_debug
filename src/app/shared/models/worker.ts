export interface Worker {
    id: number;
    firstName: string;
    secondName: string;
    email: string;
    phone: string;
    position: string;
    isActive: boolean;
}
