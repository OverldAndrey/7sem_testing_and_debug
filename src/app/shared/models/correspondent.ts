export interface Correspondent {
    id: number,
    name: string,
    email: string,
    phone: string
}
