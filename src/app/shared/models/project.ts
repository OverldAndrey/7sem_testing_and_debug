export interface Project {
    id: number;
    name: string;
    groupName: string;
    isActive: boolean;
}
