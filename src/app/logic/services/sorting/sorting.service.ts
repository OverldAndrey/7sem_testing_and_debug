import { Injectable } from '@angular/core';
import { SearchParams } from '../../../shared/models/search-params';
import { PDocument } from '../../../shared/models/document';
import { Moment } from 'moment';
import { ApiService } from '../../../data-access/services/api.service';
import { ProjectService } from '../../../data-access/services/project.service';

@Injectable({
    providedIn: 'root'
})
export class SortingService {

    constructor() { }

    async filterDocument(docs: PDocument[], params: SearchParams) {
        let res = [...docs];
        // console.log(params);

        // TODO: Enable full-text search
        if (params.query) {
            // const re = new RegExp('.*' + params.query.replace('.', '\\.') + '.*');
            res = res
                .filter(e => [e.description, e.case, e.correspondent, e.project,
                    e.worker, e.stringId, (e.parentStringId || '')]
                    .join('|').toLowerCase().includes(params.query.toLowerCase()));
        }

        if (params.type !== 'all') {
            if (params.type === 'out') {
                res = res.filter(e => e.stringId.includes('ПСТ-'));
            } else {
                res = res.filter(e => e.stringId.includes('Вх.ПС-'));
            }
        }

        if (params.createDateFrom !== undefined) {
            res = res.filter(e => (e.createDate as Moment).toDate() >= params.createDateFrom.toDate());
        }
        if (params.createDateTo !== undefined) {
            res = res.filter(e => (e.createDate as Moment).toDate() <= params.createDateTo.toDate());
        }

        const order = (params.sortOrder === 'asc') ? 1 : -1;
        let comparator;
        switch (params.sort) {
            case 'createDateSort': comparator = (a: PDocument, b: PDocument) =>
                (a.createDate as Moment).isAfter((b.createDate as Moment)) ? order : -order;
                                   break;
            case 'correspondentSort': comparator = (a: PDocument, b: PDocument) =>
                (a.correspondent + a.otherCorrespondent)
                    .localeCompare(b.correspondent + b.otherCorrespondent) * order;
                                      break;
            case 'stringIdSort': comparator = (a: PDocument, b: PDocument) =>
                a.stringId.localeCompare(b.stringId) * order;
        }
        res.sort(comparator);


        return res;
    }
}
