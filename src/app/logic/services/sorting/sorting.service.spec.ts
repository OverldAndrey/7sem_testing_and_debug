import { TestBed } from '@angular/core/testing';

import { SortingService } from './sorting.service';
import {DataBuilder, SearchParamsBuilder} from '../../../shared/mocks/data-builder';
import * as moment from 'moment';
import {PDocument} from '../../../shared/models/document';

const currentDate = moment(Date.now());

let sortingService: SortingService;
let paramsBuilder: SearchParamsBuilder;

describe('SortingService', () => {
    beforeAll(() => {
        sortingService = new SortingService();
        paramsBuilder = new SearchParamsBuilder();
    });

    it('should be created', () => {
        expect(sortingService).toBeTruthy();
    });

    it('should sort ascending by string id', async () => {
        const documents = DataBuilder.fakeDocuments(currentDate, 10);
        const params = paramsBuilder
            .setSortOrder('asc')
            .setSort('stringIdSort')
            .setType('all')
            .setQuery('')
            .extract();
        const resDocuments = documents.sort((a: PDocument, b: PDocument) =>
            a.stringId.localeCompare(b.stringId));

        const sorted = await sortingService.filterDocument(documents, params);

        expect(sorted).toEqual(resDocuments);
    });

    it('should filter by substring', async () => {
        const documents = DataBuilder.fakeDocuments(currentDate, 10);
        const params = paramsBuilder
            .setSortOrder('desc')
            .setSort('stringIdSort')
            .setType('all')
            .setQuery('ПСТ')
            .extract();
        const resDocuments = documents
            .filter(d => d.stringId.toLowerCase().includes(params.query.toLowerCase()))
            .sort((a: PDocument, b: PDocument) => -a.stringId.localeCompare(b.stringId));

        const sorted = await sortingService.filterDocument(documents, params);

        expect(sorted).toEqual(resDocuments);
    });

    afterEach(() => {
        paramsBuilder.setDefault();
    });
});
