import { TestBed } from '@angular/core/testing';

import { ExportService } from './export.service';
import { PDocument } from '../../../shared/models/document';

let exportService: ExportService;
let documents: PDocument[];
let expectedcp1251: string;
let expectedutf8: string;

describe('ExportService', () => {
    beforeAll(() => {
        exportService = new ExportService();
        documents = [
            {
                id: 1,
                project: 'qwert',
                stringId: 'ПСТ-001',
                case: 'dddddd',
                worker: 'me',
                description: 'hrewchfiweixhaifhxewdfhjxmhkfwdea',
                otherCorrespondent: '',
                parentStringId: '',
                correspondent: 'him'
            } as PDocument
        ];
        expectedcp1251 = 'data:text/csv;charset=cp1251,%31;%cf%d1%d2%2d%30%30%31;%75%6e%64%65%66%69%6e%65%64;;%68%69%6d;;%64%64%64%64%64%64;%6d%65;%71%77%65%72%74;%68%72%65%77%63%68%66%69%77%65%69%78%68%61%69%66%68%78%65%77%64%66%68%6a%78%6d%68%6b%66%77%64%65%61\r\n';
        expectedutf8 = 'data:text/csv;charset=utf-8,1%D0%9F%D0%A1%D0%A2-001undefinedhimddddddmeqwerthrewchfiweixhaifhxewdfhjxmhkfwde%0D%0A';
    });

    it('should be created', () => {
        expect(exportService).toBeTruthy();
    });

    it('should correctly encode string to cp1251', () => {
        const encoded = exportService.exportCSV(documents, 'cp1251');
        // console.log(encoded);
        expect(encoded).toEqual(expectedcp1251);
    });

    it('should correctly encode string to utf-8', () => {
        const encoded = exportService.exportCSV(documents, 'utf-8');
        // console.log(encoded);
        expect(encoded).toEqual(expectedutf8);
    });
});
