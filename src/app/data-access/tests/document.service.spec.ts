import { TestBed } from '@angular/core/testing';

import { DocumentService } from '../services/document.service';
import { DataBuilder } from '../../shared/mocks/data-builder';
import * as moment from 'moment';
import {defer, of} from 'rxjs';

let apiServiceSpy: {
    getDocuments: jasmine.Spy,
    addDocument: jasmine.Spy,
    getSingleDocument: jasmine.Spy,
    editDocument: jasmine.Spy,
    editDocumentFiles: jasmine.Spy
};
let documentService: DocumentService;
const currentDate = moment(Date.now());

describe('DocumentService', () => {
    beforeEach(() => {
        apiServiceSpy = jasmine.createSpyObj('ApiService', [
            'getDocuments',
            'addDocument',
            'getSingleDocument',
            'editDocument',
            'editDocumentFiles'
        ]);
        documentService = new DocumentService(apiServiceSpy as any);
    });

    it('should be created', () => {
        expect(documentService).toBeTruthy();
    });

    it('should fetch documents on init', async () => {
        const documents = DataBuilder.fakeDocuments(currentDate, 10);

        apiServiceSpy.getDocuments.and.returnValue(of(documents));

        await documentService.init();

        expect(documentService.fullDocuments).toEqual(documents);
    });

    it('should add document to list', async () => {
        const documents = DataBuilder.fakeDocuments(currentDate, 10);
        const newDocument = DataBuilder.fakeDocument(currentDate);

        apiServiceSpy.getDocuments.and.returnValue(of(documents));
        await documentService.init();
        apiServiceSpy.getDocuments.and.returnValue(of([...documents, newDocument]));
        apiServiceSpy.addDocument.and.returnValue(of(newDocument.stringId).toPromise());

        await documentService.addDocument(newDocument, '', []);

        expect(documentService.fullDocuments).toEqual([...documents, newDocument]);
    });

    it('should replace document in list', async () => {
        const documents = DataBuilder.fakeDocuments(currentDate, 10);
        const newDocument = [...documents][5];
        newDocument.worker = DataBuilder.fakeString();
        const editedDocs = documents.map(e => {
            if (e.id === newDocument.id) {
                return {...e, worker: newDocument.worker};
            }
            return e;
        });

        apiServiceSpy.getDocuments.and.returnValue(of(documents));
        await documentService.init();
        apiServiceSpy.getDocuments.and.returnValue(of(editedDocs));
        apiServiceSpy.editDocument.and.returnValue(defer(() => Promise.resolve()));

        await documentService.editDocument(newDocument.id, newDocument);

        expect(documentService.fullDocuments).toEqual(editedDocs);
    });

    it('should execute file edit', async () => {
        const files = [DataBuilder.fakeBlob(), DataBuilder.fakeBlob()];

        apiServiceSpy.editDocumentFiles.and.stub();

        await documentService.replaceDocumentFiles(1, files);

        expect(apiServiceSpy.editDocumentFiles.calls.count()).toEqual(1);
    });
});
