import { TestBed } from '@angular/core/testing';

import { CaseService } from '../services/case.service';
import { DataBuilder } from '../../shared/mocks/data-builder';
import { of } from 'rxjs';

let apiServiceSpy: {
    getCases: jasmine.Spy
};
let caseService: CaseService;

describe('CaseService', () => {
    beforeEach(() => {
        apiServiceSpy = jasmine.createSpyObj('ApiService', [
            'getCases'
        ]);
        caseService = new CaseService(apiServiceSpy as any);
    });

    it('should be created', () => {
        // const service: CaseService = TestBed.get(CaseService);
        expect(caseService).toBeTruthy();
    });

    it('should fetch cases on init', async () => {
        const cases = DataBuilder.fakeCases(10);

        apiServiceSpy.getCases.and.returnValue(of(cases));

        await caseService.init();

        expect(caseService.cases).toEqual(cases);
    });
});
