import { TestBed } from '@angular/core/testing';

import { ApiService } from '../services/api.service';
import * as moment from 'moment';
import { defer, Observable, of } from 'rxjs';
import { DataBuilder } from '../../shared/mocks/data-builder';
import {BrowserModule} from '@angular/platform-browser';



let httpClientSpy: {
    get: jasmine.Spy,
    post: jasmine.Spy,
    patch: jasmine.Spy
};
let apiService: ApiService;
const currentDate = moment(Date.now());


describe('ApiService', () => {
    beforeEach(() => {
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'patch']);
        apiService = new ApiService(httpClientSpy as any);
    });

    it('should be created', () => {
        // const service: ApiService = TestBed.get(ApiService);
        expect(apiService).toBeTruthy();
    });

    it('should return documents for "/rpc/getfilldocuments"', () => {
        const expectedDocuments = DataBuilder.fakeDocuments(currentDate, 5);

        // stub
        httpClientSpy.get.and.returnValue(of(expectedDocuments));

        apiService.getDocuments().subscribe(
            docs => expect(docs).toEqual(expectedDocuments), fail
        );
        // expect(httpClientSpy.get.calls.count()).toEqual(1);
    });

    it('should successfully post document for "/rpc/adddocument"', async () => {
        const document = DataBuilder.fakeDocument(currentDate);
        let resultDoc;

        // stub
        httpClientSpy.post.and.callFake((url, doc) => {
            resultDoc = doc;
            return of(document.stringId);
        });

        const ret = await apiService.addDocument(document, '', []);

        expect(ret).toEqual(document.stringId);
        // expect(resultDoc).toEqual({
        //     document: [{...document}], docrefs: [], files: []
        // });
    });

    it('should call patch document for "/documents?id=eq.${docId}"', () => {
        const document = DataBuilder.fakeDocument(currentDate);

        // mock
        httpClientSpy.patch.and.stub();

        apiService.editDocument(document.id, document);

        expect(httpClientSpy.patch.calls.count()).toEqual(1);
    });

    it('should call post files for "/rpc/replacefiles"', async () => {
        const document = DataBuilder.fakeDocument(currentDate);

        // mock
        httpClientSpy.post.and.returnValue(defer(() => Promise.resolve()));

        await apiService.editDocumentFiles(document.id, []);

        expect(httpClientSpy.post.calls.count()).toEqual(1);
    });

    it('should get document by id for "/documents?id=eq.${docId}"', () => {
        const documents = DataBuilder.fakeDocuments(currentDate, 10);
        const docId = documents[5].id;

        // stub
        httpClientSpy.get.and.returnValue(of(documents.filter(e => e.id === docId)));

        apiService.getSingleDocument(docId).subscribe(
            d => expect(d).toEqual(documents[5]), fail
        );
    });

    it('should return files for "/files?select=id,document,name,type&document=eq.${docId}"', () => {
        const files = DataBuilder.fakeFiles(10, 1);

        // stub
        httpClientSpy.get.and.returnValue(of(files));

        apiService.getFiles(1).subscribe(
            f => expect(f).toEqual(files), fail
        );
    });

    it('should return true if files exist for "/files?select=id&document=eq.${docId}"', () => {
        const files = DataBuilder.fakeFiles(3, 1);

        // stub
        httpClientSpy.get.and.returnValue(of(files));

        apiService.hasFiles(1).subscribe(
            c => expect(c).toBeTruthy(), fail
        );
    });

    it('should return false if files do not exist for "/files?select=id&document=eq.${docId}"', () => {
        const files = [];

        // stub
        httpClientSpy.get.and.returnValue(of(files));

        apiService.hasFiles(1).subscribe(
            c => expect(c).toBeFalsy(), fail
        );
    });

    it('should return projects for "/projects?select=id,name,isActive,projgroups(name)"', () => {
        const projects = DataBuilder.fakeProjects(10);
        const resProjects = projects.map(p => ({id: p.id, name: p.name, groupName: p.projgroups.name, isActive: p.isActive}));

        // stub
        httpClientSpy.get.and.returnValue(of(projects));

        apiService.getProjects().subscribe(
            pr => expect(pr).toEqual(resProjects), fail
        );
    });

    it('should return workers for "/workers?select=id,firstname,secondname,email,phone,isActive,positions(name)"', () => {
        const workers = DataBuilder.fakeWorkers(10);
        const resWorkers = workers
            .map(wr => ({...wr, firstName: wr.firstname, secondName: wr.secondname, position: wr.positions.name}));

        // stub
        httpClientSpy.get.and.returnValue(of(workers));

        apiService.getWorkers().subscribe(
            w => expect(w).toEqual(resWorkers), fail
        );
    });

    it('should return correspondents for "/correspondents"', () => {
        const corrs = DataBuilder.fakeCorrespondents(10);

        // stub
        httpClientSpy.get.and.returnValue(of(corrs));

        apiService.getCorrespondents().subscribe(
            c => expect(c).toEqual(corrs), fail
        );
    });

    it('should return cases for "/cases"', () => {
        const cases = DataBuilder.fakeCases(10);

        // stub
        httpClientSpy.get.and.returnValue(of(cases));

        apiService.getCorrespondents().subscribe(
            c => expect(c).toEqual(cases), fail
        );
    });

    it('should return documents by ref for "/rpc/getdocrefs?docid=${docId}"', () => {
        const documents = DataBuilder.fakeDocuments(currentDate, 10);
        const docId = 1;

        // stub
        httpClientSpy.get.and.returnValue(of(documents));

        apiService.getDocumentRefs(docId).subscribe(
            d => expect(d).toEqual(documents), fail
        );
    });

    it('should return downloaded files quantity for "/files?select=id,name,type&document=eq.${docId}"', async () => {
        const files = DataBuilder.fakeFiles(10);
        const docId = 1;

        // stub
        httpClientSpy.get.and.returnValue(of(files.map(f => ({id: f.id, name: f.name, type: f.type}))));
        // mock
        httpClientSpy.post.and.returnValue(defer(() => Promise.resolve()));

        const fn = await apiService.getDocumentFiles(docId);

        expect(fn).toEqual(files.length);
    });
});
