import { TestBed } from '@angular/core/testing';

import { ProjectService } from '../services/project.service';
import { DataBuilder } from '../../shared/mocks/data-builder';
import {of} from 'rxjs';

let apiServiceSpy: {
    getProjects: jasmine.Spy,
};
let projectService: ProjectService;

describe('ProjectService', () => {
    beforeEach(() => {
        apiServiceSpy = jasmine.createSpyObj('ApiService', ['getProjects']);
        projectService = new ProjectService(apiServiceSpy as any);
    });

    it('should be created', () => {
        expect(projectService).toBeTruthy();
    });

    it('should fetch projects on init', async () => {
        const projects = DataBuilder.fakeProjects(10);

        apiServiceSpy.getProjects.and.returnValue(of(projects));

        await projectService.init();

        expect(projectService.fullProjects).toEqual(projects);
    });

    it('should filter inactive projects', async () => {
        const projects = DataBuilder.fakeProjects(10);

        apiServiceSpy.getProjects.and.returnValue(of(projects));

        await projectService.init();

        expect(projectService.projects).toEqual(projects.filter(p => p.isActive));
    });
});
