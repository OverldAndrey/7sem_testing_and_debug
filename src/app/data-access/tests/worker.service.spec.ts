import { TestBed } from '@angular/core/testing';

import { WorkerService } from '../services/worker.service';
import { DataBuilder } from '../../shared/mocks/data-builder';
import { of } from 'rxjs';

let apiServiceSpy: {
    getWorkers: jasmine.Spy,
};
let workerService: WorkerService;

describe('WorkerService', () => {
    beforeEach(() => {
        apiServiceSpy = jasmine.createSpyObj('ApiService', ['getWorkers']);
        workerService = new WorkerService(apiServiceSpy as any);
    });

    it('should be created', () => {
        expect(workerService).toBeTruthy();
    });

    it('should fetch workers on init', async () => {
        const workers = DataBuilder.fakeWorkers(10);

        apiServiceSpy.getWorkers.and.returnValue(of(workers));

        await workerService.init();

        expect(workerService.fullWorkers).toEqual(workers);
    });

    it('should filter inactive workers', async () => {
        const workers = DataBuilder.fakeWorkers(10);

        apiServiceSpy.getWorkers.and.returnValue(of(workers));

        await workerService.init();

        expect(workerService.workers).toEqual(workers.filter(w => w.isActive));
    });
});
