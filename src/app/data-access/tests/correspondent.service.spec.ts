import { TestBed } from '@angular/core/testing';

import { CorrespondentService } from '../services/correspondent.service';
import { DataBuilder } from '../../shared/mocks/data-builder';
import { of } from 'rxjs';

let apiServiceSpy: {
    getCorrespondents: jasmine.Spy
};
let correspondentService: CorrespondentService;

describe('CorrespondentService', () => {
    beforeEach(() => {
        apiServiceSpy = jasmine.createSpyObj('ApiService', [
            'getCorrespondents'
        ]);
        correspondentService = new CorrespondentService(apiServiceSpy as any);
    });

    it('should be created', () => {
        expect(correspondentService).toBeTruthy();
    });

    it('should fetch correspondents on init', async () => {
        const corrs = DataBuilder.fakeCorrespondents(10);

        apiServiceSpy.getCorrespondents.and.returnValue(of(corrs));

        await correspondentService.init();

        expect(correspondentService.correspondents).toEqual(corrs);
    });

    it('should set othersName to name of lowest id', async () => {
        const corrs = DataBuilder.fakeCorrespondents(10);

        apiServiceSpy.getCorrespondents.and.returnValue(of(corrs));

        await correspondentService.init();

        expect(correspondentService.othersName).toEqual(corrs.sort((a, b) => a.id - b.id)[0].name);
    });
});
