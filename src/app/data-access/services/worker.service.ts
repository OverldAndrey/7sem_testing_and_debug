import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Worker } from '../../shared/models/worker';

@Injectable({
    providedIn: 'root'
})
export class WorkerService {
    private _workers: Worker[] = [];

    constructor(
        private api: ApiService
    ) {
        // this.init();
    }

    async init() {
        this._workers = await this.api.getWorkers().toPromise();
        this._workers = this._workers.sort((a: Worker, b: Worker) =>
            (a.secondName + ' ' + a.firstName).localeCompare(b.secondName + ' ' + b.firstName));
    }

    get workers() {
        return this._workers.filter(w => w.isActive);
    }

    get fullWorkers() {
        return this._workers;
    }
}
