import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { documentFieldNames, PDocument } from '../../shared/models/document';
import { BehaviorSubject } from 'rxjs';
import { PFile } from '../../shared/models/file';
import { Moment } from 'moment';
import * as moment from 'moment';

@Injectable({
    providedIn: 'root'
})
export class DocumentService {
    private _documents: BehaviorSubject<PDocument[]> = new BehaviorSubject<PDocument[]>([]);
    private _fullDocuments: PDocument[] = [];
    public maxInNumber = 0;
    public maxOutNumber = 0;

    constructor(
        private api: ApiService
    ) {
        // this.init(); alter table dbcourse.documents add constraint stringIdUnique unique ("stringId");
    }

    async init() {
        this._fullDocuments = await this.api.getDocuments().toPromise();
        const comparator = (a: PDocument, b: PDocument) =>
            (a.createDate as Moment).isAfter((b.createDate as Moment)) ? -1 : 1;
        this._fullDocuments.sort(comparator);
        this._documents.next(this._fullDocuments);

        if (this.fullDocuments.length === 0) {
            return;
        } else if (this.fullDocuments[0].stringId.includes('Вх.ПС')) {
            if ((this.fullDocuments[0].createDate as Moment).year() === moment(Date.now()).year()) {
                this.maxInNumber = parseInt(this.fullDocuments[0].stringId.slice(6), 10);
            }
            let i = 0;
            for (; this.fullDocuments[i].stringId.includes('Вх.ПС') && i < this.fullDocuments.length; i++) { }
            if (i < this.fullDocuments.length) {
                this.maxOutNumber = parseInt(this.fullDocuments[i].stringId.slice(4), 10);
            }
        } else {
            if ((this.fullDocuments[0].createDate as Moment).year() === moment(Date.now()).year()) {
                this.maxOutNumber = parseInt(this.fullDocuments[0].stringId.slice(4), 10);
            }
            let i = 0;
            for (; this.fullDocuments[i].stringId.includes('ПСТ') && i < this.fullDocuments.length; i++) { }
            if (i < this.fullDocuments.length) {
                this.maxInNumber = parseInt(this.fullDocuments[i].stringId.slice(6), 10);
            }
        }
    }

    get documents() {
        return this._documents;
    }

    get fullDocuments() {
        return this._fullDocuments;
    }

    private readFileAsync(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();

            reader.onload = () => {
                resolve(reader.result);
            };

            reader.onerror = reject;

            reader.readAsArrayBuffer(file);
        });
    }

    public async addDocument(doc: PDocument, docRefs: string = '', files?) {
        const pfiles: PFile[] = [];

        for (const f of files) {
            const d = await this.readFileAsync(f);
            // @ts-ignore
            const darr = [...(new Uint8Array(d))];
            pfiles.push({
                    document: doc.id,
                    name: f.name,
                    type: f.type,
                    data: '\\x' + darr
                        // tslint:disable-next-line:radix
                        .map(e => parseInt(e).toString(16))
                        .map((e: string) => e.length === 2 ? e : '0' + e)
                        .join('') as string
                }
            );
        }

        const resStringId = await this.api.addDocument(doc, docRefs, pfiles);

        this._fullDocuments = await this.api.getDocuments().toPromise();
        const comparator = (a: PDocument, b: PDocument) =>
            (a.createDate as Moment).isAfter((b.createDate as Moment)) ? -1 : 1;
        this._fullDocuments.sort(comparator);
        this._documents.next(this._fullDocuments);

        resStringId.slice(0, 3) === 'ПСТ' ?
            this.maxOutNumber = +resStringId.slice(4) : this.maxInNumber = +resStringId.slice(6);
        // this.maxId = Math.max(...this._fullDocuments.map(e => e.id));
    }

    public async getDocument(docId: number) {
        return await this.api.getSingleDocument(docId).toPromise();
    }

    public async editDocument(docId: number, doc: any) {
        await this.api.editDocument(docId, doc).toPromise();
        this._fullDocuments = await this.api.getDocuments().toPromise();
        const comparator = (a: PDocument, b: PDocument) =>
            (a.createDate as Moment).isAfter((b.createDate as Moment)) ? -1 : 1;
        this._fullDocuments.sort(comparator);
        this._documents.next(this._fullDocuments);
    }

    public async replaceDocumentFiles(docId: number, files?) {
        const pfiles: PFile[] = [];

        for (const f of files) {
            const d = await this.readFileAsync(f);
            // @ts-ignore
            const darr = [...(new Uint8Array(d))];
            pfiles.push({
                    document: docId,
                    name: f.name,
                    type: f.type,
                    data: '\\x' + darr
                        .map(e => parseInt(e).toString(16))
                        .map((e: string) => e.length === 2 ? e : '0' + e)
                        .join('') as string
                }
            );
        }

        await this.api.editDocumentFiles(docId, pfiles);
    }

    public async getDocumentFiles(docId: number) {
        return this.api.getDocumentFiles(docId);
    }
}
