import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Case } from '../../shared/models/case';

@Injectable({
    providedIn: 'root'
})
export class CaseService {
    private _cases: Case[] = [];

    constructor(
        private api: ApiService
    ) {
        // this.init();
    }

    async init() {
        this._cases = await this.api.getCases().toPromise();
    }

    get cases() {
        return this._cases;
    }
}
