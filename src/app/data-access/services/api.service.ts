import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PDocument } from '../../shared/models/document';
import * as moment from 'moment';
import { map, tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Project } from '../../shared/models/project';
import { Worker } from '../../shared/models/worker';
import { Correspondent } from '../../shared/models/correspondent';
import { Case } from '../../shared/models/case';
import { PFile } from '../../shared/models/file';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    constructor(
        private http: HttpClient
    ) { }

    public getDocuments(): Observable<PDocument[]> {
        // console.log(environment);
        return this.http.get<PDocument[]>(environment.databaseURL + '/rpc/getfulldocuments').pipe(
            map(e => e.map((d: PDocument) => ({
                ...d,
                createDate: d.createDate ? moment(d.createDate) : ''
            }))),
            // tap(console.log)
        );
    }

    public async addDocument(doc: PDocument, docRefs: string = '', files: PFile[]) {
        const res = {...doc};

        // res.stringId = 'ПСТ-020';

        const refs = [];
        if (docRefs !== '') {
            for (let s of docRefs.split(' ')) {
                refs.push({primedoc: 0, refdoc: +s});
            }
        }
        for (let f of files) {
            f.document = 0;
        }
        let r: string | null = null;
        while (!r) {
            try {
                r = await this.http.post<string>(environment.databaseURL + '/rpc/adddocument', {
                    document: [{...res}],
                    docrefs: refs,
                    files
                }).toPromise();
            } catch (e) {
                if (e.error.code === '23505' && (e.error.message as string)
                    .includes('duplicate key value violates unique constraint "documents_stringId_key"')) {
                    console.log(`Constraint violation, retrying (${res.stringId})`);
                    res.stringId = (res.stringId.slice(0, 6) === 'Вх.ПС-' ?
                        'Вх.ПС-' + ('00' + (+res.stringId.slice(-3) + 1)).slice(-3) :
                        'ПСТ-' + ('00' + (+res.stringId.slice(-3) + 1)).slice(-3));
                } else {
                    console.log(e);
                    r = 'Error';
                    throw e;
                }
            }
        }
        // console.log(r);
        return r;
    }

    public editDocument(docId: number, doc: any) {
        return this.http.patch(environment.databaseURL + `/documents?id=eq.${docId}`, doc);
    }

    public async editDocumentFiles(docid: number, files) {
        await this.http.post(environment.databaseURL + '/rpc/replacefiles', {
            docid,
            files
        }).toPromise();
    }

    public getSingleDocument(docId: number): Observable<PDocument> {
        return this.http.get<PDocument[]>(environment.databaseURL + `/documents?id=eq.${docId}`)
            .pipe(map(e => e[0]));
    }

    public getFiles(docId: number): Observable<PFile[]> {
        return this.http.get<PFile[]>(environment.databaseURL + `/files?select=id,document,name,type&document=eq.${docId}`);
    }

    public hasFiles(docId: number): Observable<boolean> {
        return this.http.get(environment.databaseURL + `/files?select=id&document=eq.${docId}`).pipe(
                map((e: PFile[]) => e.length > 0)
            );
    }

    public getProjects(): Observable<Project[]> {
        return this.http.get<Project[]>(environment.databaseURL + '/projects?select=id,name,isActive,projgroups(name)').pipe(
            map(e => e.map((pr: any) =>
                ({id: pr.id, name: pr.name, groupName: pr.projgroups.name, isActive: pr.isActive} as Project)))
        );
    }

    public getWorkers(): Observable<Worker[]> {
        return this.http.get<Worker[]>(
            environment.databaseURL + '/workers?select=id,firstname,secondname,email,phone,isActive,positions(name)').pipe(
            map(e => e.map(
                (wrk: any) => ({
                    ...wrk,
                    firstName: wrk.firstname,
                    secondName: wrk.secondname,
                    position: wrk.positions.name
                } as Worker)
            ))
        );
    }

    public getCorrespondents(): Observable<Correspondent[]> {
        return this.http.get<Correspondent[]>(environment.databaseURL + '/correspondents');
    }

    public getCases(): Observable<Case[]> {
        return this.http.get<Case[]>(environment.databaseURL + '/cases');
    }

    public getDocumentRefs(docId: number): Observable<PDocument[]> {
        return this.http.get<PDocument[]>(environment.databaseURL + `/rpc/getdocrefs?docid=${docId}`);
    }

    public async getDocumentFiles(docId: number): Promise<number> {
        const files: any[] = await this.http.get<any[]>(
            environment.databaseURL + `/files?select=id,name,type&document=eq.${docId}`).toPromise();

        // TODO: testing needed
        for (let f of files) {
            const headers = new HttpHeaders({
                'Accept': 'application/octet-stream'
            });
            const res = await this.http.post(environment.databaseURL + `/rpc/getfile?select=data`, {fid: f.id}, {
                headers,
                responseType: 'blob'
            }).toPromise();
            // console.log(res);

            try {
                const a = document.createElement('a');
                document.body.appendChild(a);
                const url = window.URL.createObjectURL(res);
                a.href = url;
                a.download = f.name;
                a.click();
                window.URL.revokeObjectURL(url);
                document.body.removeChild(a);
            } catch (e) {
                console.log('Global objects somehow unavailable');
            }
        }
        return files.length;
    }
}

