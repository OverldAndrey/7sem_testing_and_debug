import { Injectable } from '@angular/core';
import { Project } from '../../shared/models/project';
import {ApiService} from './api.service';

@Injectable({
    providedIn: 'root'
})
export class ProjectService {
    private _projects: Project[] = [];

    constructor(
        private api: ApiService
    ) {
        // this.init();
    }

    async init() {
        this._projects = await this.api.getProjects().toPromise();
    }

    get projects() {
        return this._projects.filter(p => p.isActive);
    }

    get fullProjects() {
        return this._projects;
    }
}
