import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Correspondent } from '../../shared/models/correspondent';

@Injectable({
    providedIn: 'root'
})
export class CorrespondentService {
    private _correspondents: Correspondent[] = [];

    othersName = 'Прочие';

    constructor(
        private api: ApiService
    ) {
        // this.init();
    }

    async init() {
        this._correspondents = await this.api.getCorrespondents().toPromise();
        this.othersName = this._correspondents
            .sort((a, b) => a.id - b.id)[0].name;
    }

    get correspondents() {
        return this._correspondents;
    }
}
