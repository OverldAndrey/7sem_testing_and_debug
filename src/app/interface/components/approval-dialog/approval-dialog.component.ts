import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
    selector: 'app-approval-dialog',
    templateUrl: './approval-dialog.component.html',
    styleUrls: ['./approval-dialog.component.scss']
})
export class ApprovalDialogComponent implements OnInit {

    constructor(
        public dialogRef: MatDialogRef<ApprovalDialogComponent>,
    ) { }

    ngOnInit() {
    }

}
