import { Component, Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import { documentFieldNames, documentFieldNamesRus, PDocument } from '../../../shared/models/document';
import { DatePipe } from '@angular/common';
import { ApiService } from '../../../data-access/services/api.service';
import {EditDocumentCardComponent} from '../edit-document-card/edit-document-card.component';
import {CorrespondentService} from '../../../data-access/services/correspondent.service';

@Component({
  selector: 'app-document-details',
  templateUrl: './document-details.component.html',
  styleUrls: ['./document-details.component.scss']
})
export class DocumentDetailsComponent implements OnInit {
    nameMap = documentFieldNames
        .map((e, i) => ({key: e, val: documentFieldNamesRus[i]}))
        .filter(e => e.key !== 'otherCorrespondent')
        .concat([{key: 'files', val: 'Файлы'}]);
    documentData = [];
    documentType = 'out';
    filesString = '';

    constructor(
        public dialogRef: MatDialogRef<DocumentDetailsComponent>,
        @Inject(MAT_DIALOG_DATA) public doc: {doc: PDocument},
        public dialog: MatDialog,
        public api: ApiService,
        public correspondent: CorrespondentService
    ) { }

    ngOnInit() {
        this.documentType = this.doc.doc.stringId.includes('Вх') ? 'in' : 'out';
        this.setFormattedDoc(this.doc.doc);
    }

    setFormattedDoc(doc: PDocument) {
        const res = {...doc} as any;
        // console.log(res);
        const dp = new DatePipe('en-US');
        if (res.createDate !== undefined) {
            res.createDate = dp.transform(res.createDate, 'dd/MM/yyyy H:mm:ss');
        }
        if (res.correspondent.includes(this.correspondent.othersName)) {
            res.correspondent = 'Прочие, ' + res.otherCorrespondent;
        }
        res.correspondent = res.correspondent.replace('<>', '');
        res.worker = res.worker.replace('<>', '');
        this.api.getFiles(res.id).subscribe(e => {
            res['files'] = e.map(f => f.name).join('; ');
            this.filesString = res['files'];
            this.documentData = this.nameMap
                .map(el => ({tkey: el.val, tval: res[el.key]}));
        });
    }

    openEditDocument() {
        const editDialog = this.dialog.open(EditDocumentCardComponent,
            {
                data: {...this.doc.doc, files: this.filesString},
                disableClose: true
            });
        editDialog.afterClosed().subscribe(res => {
            if (res) {
                this.setFormattedDoc(res);
            }
        });
    }

    onClose() {
        this.dialogRef.close();
    }
}
