import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CorrespondentService } from '../../../data-access/services/correspondent.service';
import { WorkerService } from '../../../data-access/services/worker.service';
import { ProjectService } from '../../../data-access/services/project.service';
import { CaseService } from '../../../data-access/services/case.service';
import { PDocument } from '../../../shared/models/document';
import { DocumentService } from '../../../data-access/services/document.service';
import * as moment from 'moment';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { documentFieldNamesRus } from '../../../shared/models/document';
import { Moment } from 'moment';
import { ApprovalDialogComponent } from '../approval-dialog/approval-dialog.component';

@Component({
    selector: 'app-add-document-card',
    templateUrl: './add-document-card.component.html',
    styleUrls: ['./add-document-card.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class AddDocumentCardComponent implements OnInit {
    INVALID_FORM_MESSAGE_EMPTY = 'Неверный ввод данных. Некоторые поля не заполнены.';
    INVALID_FORM_MESSAGE_WRONG_REFS = 'Неверный ввод данных. В поле "Связанные документы" должны быть только номера (№).';
    INVALID_FORM_MESSAGE_NONEXISTENT_REFS = 'Неверный ввод данных. Документов с введенными' +
        ' в поле "Связанные документы" номерами не существует.';

    fieldNames = documentFieldNamesRus;
    documentStringId = (this.data.type === 'in' ?
        'Вх.ПС-' + ('00' + (this.documents.maxInNumber + 1)).slice(-3) :
        'ПСТ-' + ('00' + (this.documents.maxOutNumber + 1)).slice(-3));

    initValue = {
        stringId: this.documentStringId,
        parentStringId: '',
        correspondent: '',
        otherCorrespondent: '',
        worker: '',
        project: '',
        case: '',
        description: '',
        files: null
    };

    hasChanged = false;

    addDocForm = new FormGroup({
        stringId: new FormControl(this.documentStringId, Validators.required),
        parentStringId: new FormControl(''),
        correspondent: new FormControl('', Validators.required),
        otherCorrespondent: new FormControl(''),
        worker: new FormControl('', Validators.required),
        project: new FormControl('', Validators.required),
        case: new FormControl('', Validators.required),
        description: new FormControl('', Validators.required),
        files: new FormControl(null)
    });

    constructor(
        public correspondents: CorrespondentService,
        public workers: WorkerService,
        public projects: ProjectService,
        public cases: CaseService,
        private documents: DocumentService,
        public dialog: MatDialog,
        public dialogRef: MatDialogRef<AddDocumentCardComponent>,
        @Inject(MAT_DIALOG_DATA) public data: {type: 'in' | 'out'}
    ) { }

    ngOnInit() {
        this.addDocForm.setValue(this.initValue);
        this.addDocForm.valueChanges.subscribe(() => this.hasChanged = true);
        // console.log(this.projects.fullProjects);
    }

    onDocumentTypeChange(type: 'in' | 'out') {
        this.data.type = type;
        this.documentStringId = (this.data.type === 'in' ?
            'Вх.ПС-' + ('00' + (this.documents.maxInNumber + 1)).slice(-3) :
            'ПСТ-' + ('00' + (this.documents.maxOutNumber + 1)).slice(-3));
        this.initValue = {
            stringId: this.documentStringId,
            parentStringId: '',
            correspondent: '',
            otherCorrespondent: '',
            worker: '',
            project: '',
            case: '',
            description: '',
            files: null
        };
        this.addDocForm.setValue(this.initValue);
        if (this.data.type === 'out' && document.getElementById('refDocs')) {
            document.getElementById('refDocs')['value'] = '';
        }
        this.hasChanged = false;
    }

    // onAdministrative() {
    //     if (this.addDocForm.value['isAdministrative'] === true) {
    //         this.documentStringId += 'дсп';
    //         this.addDocForm.controls['stringId'].setValue(this.documentStringId);
    //     } else {
    //         this.documentStringId = this.initValue.stringId;
    //         this.addDocForm.controls['stringId'].setValue(this.documentStringId);
    //     }
    //     console.log(this.addDocForm.value);
    // }

    async processAddDocument() {
        if (this.addDocForm.valid) {
            let docRefs = '';

            if (this.data.type === 'out') {
                // tslint:disable-next-line:no-string-literal
                docRefs = document.getElementById('refDocs')['value'] as string;
                const refs = [];
                if (docRefs !== '') {
                    const strRefs = docRefs.split(' ');
                    if (!strRefs.reduce((r, e) => r && !isNaN(+e), true)) {
                        console.log('Invalid form!');
                        alert(this.INVALID_FORM_MESSAGE_WRONG_REFS);
                        return;
                    }
                    for (const s of strRefs) {
                        refs.push({primedoc: 0, refdoc: +s});
                    }
                    if (refs.some(e => !this.documents.fullDocuments.map(d => d.id).includes(e.refdoc))) {
                        console.log('Invalid form!');
                        alert(this.INVALID_FORM_MESSAGE_NONEXISTENT_REFS);
                        return;
                    }
                }
            }

            const doc = this.addDocForm.value;
            let f = [];
            if (doc.files) {
                f = doc.files._files;
            }
            doc.files = undefined;
            doc.parentStringId = doc.parentStringId !== '' ? doc.parentStringId : null;
            doc.createDate = moment(Date.now());
            // console.log(doc);
            await this.documents.addDocument(doc as PDocument, docRefs, f);
            // this.data.type === 'out' ? this.documents.maxOutNumber++ : this.documents.maxInNumber++;
            this.dialogRef.close();
        } else {
            console.log('Invalid form!');
            alert(this.INVALID_FORM_MESSAGE_EMPTY);
        }
    }

    onClose() {
        if (this.hasChanged ||
            (document.getElementById('refDocs') && document.getElementById('refDocs')['value'] !== '')) {
            this.dialog.open(ApprovalDialogComponent).afterClosed()
                .subscribe(res => res ? this.dialogRef.close() : null);
        } else {
            this.dialogRef.close();
        }
    }
}
