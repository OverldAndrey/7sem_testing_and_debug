import { Component, ElementRef, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ProjectService } from '../../../data-access/services/project.service';

@Component({
  selector: 'app-search-params',
  templateUrl: './search-params.component.html',
  styleUrls: ['./search-params.component.scss']
})
export class SearchParamsComponent implements OnInit {
    @Output() paramsChange = new EventEmitter<any>();

    docSearch = new FormGroup({
        query: new FormControl(''),
        type: new FormControl('all'),
        fromCreateTime: new FormControl(''),
        toCreateTime: new FormControl(''),
        sortOrder: new FormControl('desc'),
        sort: new FormControl('createDateSort')
    });

    constructor(
        // public projects: ProjectService
    ) { }

    ngOnInit() {
        this.docSearch.valueChanges.subscribe(e => {
            this.docSearch.valid ? this.paramsChange.emit(e) : this.docSearch.reset({
                ...e
            });
        });
    }

    resetFilters() {
        this.docSearch.reset({}, {emitEvent: false});
        // this.docSearch.controls['project'].setValue(0, {emitEvent: false});
        this.docSearch.controls['sortOrder'].setValue('desc', {emitEvent: false});
        this.docSearch.controls['type'].setValue('all');
    }

    changeSortOrder() {
        const changedOrder = (this.docSearch.controls['sortOrder'].value === 'asc') ? 'desc' : 'asc';
        this.docSearch.controls['sortOrder'].setValue(changedOrder);
    }
}
