import { Component, Input, OnInit } from '@angular/core';
import { DocumentService } from '../../../data-access/services/document.service';
import { documentFieldNames, documentFieldNamesRus, PDocument } from '../../../shared/models/document';
import { MatDialog } from '@angular/material/dialog';
import { DocumentDetailsComponent } from '../document-details/document-details.component';
import { ApiService } from '../../../data-access/services/api.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CorrespondentService } from '../../../data-access/services/correspondent.service';

@Component({
  selector: 'app-document-table',
  templateUrl: './document-table.component.html',
  styleUrls: ['./document-table.component.scss']
})
export class DocumentTableComponent implements OnInit {
    columnNames = documentFieldNames
        .filter(e => e !== 'case' && e !== 'parentStringId' &&
            e !== 'otherCorrespondent' && e !== 'id');
    nameMap = documentFieldNames
        .map((e, i) => ({key: e, val: documentFieldNamesRus[i]}))
        .filter((e) => e.key !== 'createDate' && e.key !== 'worker' &&
            e.key !== 'correspondent');
    @Input() docs: PDocument[];

    constructor(
        private documents: DocumentService,
        public correspondents: CorrespondentService,
        private api: ApiService,
        public snackBar: MatSnackBar,
        public dialog: MatDialog
    ) { }

    ngOnInit() {
    }

    async hasFiles(docId: number): Promise<boolean> {
        return await this.api.hasFiles(docId).toPromise();
    }

    onGetFiles(docId: number) {
        this.documents.getDocumentFiles(docId).then(
            n => {
                let message = '';
                if (n === 0) {
                    message = 'К данному документу не прикреплено файлов';
                } else {
                    const lastN = +n.toString().slice(-1);
                    message = `Загружен${(lastN !== 1) ? 'о' : ''} ${n}` +
                        ` файл${(lastN !== 1) ? ((lastN < 5 && lastN !== 0) ? 'а' : 'ов') : ''}`;
                    // console.log(lastN);
                }
                this.snackBar.open(
                    message,
                    'Закрыть',
                {duration: 4000}
                );
            }
        );
    }

    onShowDocumentDetails(doc) {
        const res = {...doc};
        this.dialog.open(DocumentDetailsComponent, {
            data: {
                doc: res // {...doc, isSent: (doc.isSent) ? 'Да' : 'Нет', isControlled: (doc.isControlled) ? 'Да' : 'Нет'}
            }
        });
    }
}
