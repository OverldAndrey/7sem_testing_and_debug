import { Component, Inject, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { documentFieldNamesRus, PDocument } from '../../../shared/models/document';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CorrespondentService } from '../../../data-access/services/correspondent.service';
import { WorkerService } from '../../../data-access/services/worker.service';
import { DocumentService } from '../../../data-access/services/document.service';

@Component({
    selector: 'app-edit-document-card',
    templateUrl: './edit-document-card.component.html',
    styleUrls: ['./edit-document-card.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class EditDocumentCardComponent implements OnInit {
    fieldNames = documentFieldNamesRus;
    filesString = '';
    filesChanged = false;

    editDocForm = new FormGroup({
        description: new FormControl('', Validators.required),
        correspondent: new FormControl('', Validators.required),
        otherCorrespondent: new FormControl(''),
        worker: new FormControl('', Validators.required),
        clearFiles: new FormControl(false),
        files: new FormControl(null)
    });

    constructor(
         public dialogRef: MatDialogRef<EditDocumentCardComponent>,
         @Inject(MAT_DIALOG_DATA) public doc: any,
         public correspondents: CorrespondentService,
         public workers: WorkerService,
         public documents: DocumentService
    ) { }

    ngOnInit() {
        this.documents.getDocument(this.doc.id).then((res) => {
            // console.log(this.doc);
            this.editDocForm.setValue({
                description: res.description,
                correspondent: res.correspondent,
                otherCorrespondent: res.otherCorrespondent,
                worker: res.worker,
                clearFiles: false,
                files: null
            });
            this.filesString = this.doc.files;
        });
    }

    onClose() {
         this.dialogRef.close(null);
    }

    async processEditDocument() {
        if (this.editDocForm.valid) {
            const editDoc = this.editDocForm.value;
            const clearFiles = editDoc.clearFiles;
            let f = [];
            if (editDoc.files) {
                f = editDoc.files._files;
            }
            editDoc.files = undefined;
            editDoc.clearFiles = undefined;
            await this.documents.editDocument(this.doc.id, this.editDocForm.value);
            if (f.length > 0 || clearFiles) {
                await this.documents.replaceDocumentFiles(this.doc.id, f).catch(console.log);
            }
            this.dialogRef.close(this.documents.fullDocuments.filter(e => e.id === this.doc.id)[0]);
        } else {
            console.log('Invalid form!');
            alert('Неверный ввод данных. Некоторые поля не заполнены.');
        }
    }

    onFileChange() {
        this.filesChanged = true;
    }
}
