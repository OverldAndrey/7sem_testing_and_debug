import { Component, OnInit } from '@angular/core';
import { DocumentService } from '../../../data-access/services/document.service';
import { MatDialog } from '@angular/material/dialog';
import { AddDocumentCardComponent } from '../add-document-card/add-document-card.component';
import { SortingService } from '../../../logic/services/sorting/sorting.service';
import { SearchParams } from '../../../shared/models/search-params';
import * as moment from 'moment';
import { ExportService } from '../../../logic/services/export/export.service';
import { HelpComponent } from '../help/help.component';
import { environment } from '../../../../environments/environment';

@Component({
    selector: 'app-main',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
    docs = this.documents.documents;

    constructor(
        public dialog: MatDialog,
        private documents: DocumentService,
        public search: SortingService,
        private exporter: ExportService
    ) { }

    ngOnInit() {
    }

    openDocumentAdding(type: 'in' | 'out') {
        this.dialog.open(AddDocumentCardComponent, {data: {type}, disableClose: true});
    }

    openHelp() {
        this.dialog.open(HelpComponent);
    }

    openAdmin() {
        window.open(environment.adminURL, '_blank');
    }

    downloadTable(fileType: 'csv') {
        const file = this.exporter.exportCSV(this.documents.documents.value, 'cp1251');
        this.exporter.downloadFileFromURI(file);
    }

    onSearchParams(ev) {
        const params: SearchParams = {
            query: ev.query,
            type: ev.type,
            createDateFrom: ev.fromCreateTime ? moment(ev.fromCreateTime) : undefined,
            createDateTo: ev.toCreateTime ? moment(ev.toCreateTime).add(23, 'hours').add(59, 'minutes').add(59, 'seconds') : undefined,
            sortOrder: ev.sortOrder,
            sort: ev.sort
        };
        // console.log(params);
        this.search.filterDocument(this.documents.fullDocuments, params).then(e => this.docs.next(e));
    }
}
