import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentDetailsComponent } from '../components/document-details/document-details.component';
import { MaterialProxyModule } from '../../material-proxy/material-proxy.module';
import { DataBuilder } from '../../shared/mocks/data-builder';
import * as moment from 'moment';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CorrespondentService } from '../../data-access/services/correspondent.service';
import {of} from 'rxjs';
import {HAMMER_LOADER} from '@angular/platform-browser';

const currentDate = moment(Date.now());

class MockDialogRef {
    count = 0;

    close() {
        this.count++;
    }
}

class CorrespondentServiceMock {
    otherName = DataBuilder.fakeString();
}

class MatDialogMock {
    count = 0;

    open(component: any, config: any) {
        this.count++;

        return {
            afterClosed: () => {
                return of(DataBuilder.fakeDocument(currentDate));
            }
        };
    }
}

describe('DocumentDetailsComponent', () => {
    let component: DocumentDetailsComponent;
    let fixture: ComponentFixture<DocumentDetailsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ DocumentDetailsComponent ],
            imports: [
                MaterialProxyModule,
                ReactiveFormsModule,
                HttpClientModule,
            ],
            providers: [
                { provide: MatDialogRef, useClass: MockDialogRef  },
                { provide: MAT_DIALOG_DATA, useValue: { doc: DataBuilder.fakeDocument(currentDate) } },
                { provide: CorrespondentService, useClass: CorrespondentServiceMock },
                { provide: MatDialog, useClass: MatDialogMock },
                {
                    provide: HAMMER_LOADER,
                    useValue: () => new Promise(() => {})
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DocumentDetailsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should open dialog', () => {
        component.dialog.open(DocumentDetailsComponent, {});

        // @ts-ignore
        expect(component.dialog.count).toEqual(1);
    });

    afterEach(() => {
        TestBed.resetTestingModule();
    });
});
