import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDocumentCardComponent } from '../components/add-document-card/add-document-card.component';
import { MaterialProxyModule } from '../../material-proxy/material-proxy.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {BrowserModule, HAMMER_LOADER} from '@angular/platform-browser';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DocumentService } from '../../data-access/services/document.service';
import { DataBuilder } from '../../shared/mocks/data-builder';
import * as moment from 'moment';
import { PDocument } from '../../shared/models/document';
import { PFile } from '../../shared/models/file';

const currentDate = moment(Date.now());

class MockDialogRef {
    count = 0;

    close() {
        this.count++;
    }
}

class DocumentServiceMock {
    docs = DataBuilder.fakeDocuments(moment(Date.now()), 10);

    get fullDocuments() {
        return this.docs;
    }

    addDocument(doc: PDocument, drs: string, f: PFile) {
        return new Promise((res, rej) => {
            res();
        });
    }
}

describe('AddDocumentCardComponent', () => {
    let component: AddDocumentCardComponent;
    let fixture: ComponentFixture<AddDocumentCardComponent>;
    let documentsService: DocumentService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ AddDocumentCardComponent ],
            imports: [
                BrowserModule,
                MaterialProxyModule,
                ReactiveFormsModule,
                HttpClientModule,
                BrowserAnimationsModule,
            ],
            providers: [
                { provide: MatDialogRef, useClass: MockDialogRef  },
                { provide: MAT_DIALOG_DATA, useValue: { type: 'in' } },
                { provide: DocumentService, useClass: DocumentServiceMock },
                {
                    provide: HAMMER_LOADER,
                    useValue: () => new Promise(() => {})
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AddDocumentCardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();

        documentsService = TestBed.get(DocumentService);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should execute dialog close after add document', async () => {
        const res = {...DataBuilder.fakeDocument(currentDate), files: null};
        delete res.id;
        delete res.createDate;
        component.addDocForm.setValue({...res, files: null});
        await component.processAddDocument();

        // @ts-ignore
        expect(component.dialogRef.count).toEqual(1);
    });

    it('should execute dialog close on close event', () => {
        component.onClose();

        // @ts-ignore
        expect(component.dialogRef.count).toEqual(1);
    });

    it('should reset form on type change (to "in")', () => {
        component.onDocumentTypeChange('in');

        expect(component.addDocForm.getRawValue()).toEqual({
            stringId: component.documentStringId,
            parentStringId: '',
            correspondent: '',
            otherCorrespondent: '',
            worker: '',
            project: '',
            case: '',
            description: '',
            files: null
        });
    });

    afterEach(() => {
        TestBed.resetTestingModule();
    });

    // afterEach(() => {
    //     // @ts-ignore
    //     component.dialog.count = 0;
    // });
});
