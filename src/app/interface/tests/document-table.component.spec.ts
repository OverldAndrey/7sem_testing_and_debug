import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { DocumentTableComponent } from '../components/document-table/document-table.component';
import { MaterialProxyModule } from '../../material-proxy/material-proxy.module';
import { of } from 'rxjs';
import { DataBuilder } from '../../shared/mocks/data-builder';
import * as moment from 'moment';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {CorrespondentService} from '../../data-access/services/correspondent.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {DocumentService} from '../../data-access/services/document.service';
import {PDocument} from '../../shared/models/document';
import {PFile} from '../../shared/models/file';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

const currentDate = moment(Date.now());

class MatDialogMock {
    count = 0;

    open(component: any, config: any) {
        this.count++;
    }
}

class MatSnackBarMock {
    count = 0;

    open(msg: any, act: any, config: any) {
        this.count++;
    }
}

class DocumentServiceMock {
    async getDocumentFiles(id: number) {
        return DataBuilder.fakeNumber();
    }
}

describe('DocumentTableComponent', () => {
    let component: DocumentTableComponent;
    let fixture: ComponentFixture<DocumentTableComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ DocumentTableComponent ],
            imports: [
                MaterialProxyModule,
                ReactiveFormsModule,
                HttpClientModule,
            ],
            providers: [
                { provide: MatDialog, useClass: MatDialogMock },
                { provide: MatSnackBar, useClass: MatSnackBarMock },
                { provide: DocumentService, useClass: DocumentServiceMock }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DocumentTableComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should open file notification snackbar', fakeAsync(() => {
        component.onGetFiles(3);

        tick(200);

        // @ts-ignore
        expect(component.snackBar.count).toEqual(1);
    }));

    it('should open details dialog', () => {
        component.onShowDocumentDetails(DataBuilder.fakeDocument(currentDate));

        // @ts-ignore
        expect(component.dialog.count).toEqual(1);
    });

    afterEach(() => {
        TestBed.resetTestingModule();
    });
});
