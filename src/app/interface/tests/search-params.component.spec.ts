import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { SearchParamsComponent } from '../components/search-params/search-params.component';
import { MaterialProxyModule } from '../../material-proxy/material-proxy.module';
import { FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HAMMER_LOADER } from '@angular/platform-browser';

describe('SearchParamsComponent', () => {
    let component: SearchParamsComponent;
    let fixture: ComponentFixture<SearchParamsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ SearchParamsComponent ],
            imports: [
                MaterialProxyModule,
                ReactiveFormsModule,
                HttpClientModule,
                BrowserAnimationsModule
            ],
            providers: [
                {
                    provide: HAMMER_LOADER,
                    useValue: () => new Promise(() => {})
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SearchParamsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should reset filters on reset filters event', fakeAsync(() => {
        component.resetFilters();

        tick(200);

        expect(component.docSearch.getRawValue()).toEqual({
            query: null,
            type: 'all',
            fromCreateTime: null,
            toCreateTime: null,
            sortOrder: 'desc',
            sort: null
        });
    }));

    it('should change sort order on change sort order event', fakeAsync(() => {
        component.changeSortOrder();

        tick(200);

        expect(component.docSearch.controls['sortOrder'].value).toEqual('asc');
    }));

    afterEach(() => {
        TestBed.resetTestingModule();
    });
});
