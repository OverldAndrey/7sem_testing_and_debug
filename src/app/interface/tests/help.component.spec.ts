import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpComponent } from '../components/help/help.component';
import { MaterialProxyModule } from '../../material-proxy/material-proxy.module';

describe('HelpComponent', () => {
    let component: HelpComponent;
    let fixture: ComponentFixture<HelpComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ HelpComponent ],
            imports: [ MaterialProxyModule ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HelpComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    afterEach(() => {
        TestBed.resetTestingModule();
    });
});
