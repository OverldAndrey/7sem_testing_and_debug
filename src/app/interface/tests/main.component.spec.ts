import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainComponent } from '../components/main/main.component';
import { MaterialProxyModule } from '../../material-proxy/material-proxy.module';
import * as moment from 'moment';
import {DataBuilder} from '../../shared/mocks/data-builder';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {DocumentService} from '../../data-access/services/document.service';
import {BehaviorSubject} from 'rxjs';
import {Component, Input} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {SortingService} from '../../logic/services/sorting/sorting.service';
import {HAMMER_LOADER} from '@angular/platform-browser';

const currentDate = moment(Date.now());

class MatDialogMock {
    count = 0;

    open(component: any, config: any) {
        this.count++;
    }
}

class DocumentServiceMock {
    docs = new BehaviorSubject(DataBuilder.fakeDocuments(moment(Date.now()), 10));

    get documents() {
        return this.docs;
    }
}

class SortingServiceMock {
    count = 0;

    async filterDocument(docs: any, params: any) {
        this.count++;
        return DataBuilder.fakeDocuments(currentDate, 10);
    }
}

@Component({selector: 'app-search-params', template: ''})
class SearchParamsStubComponent {}

@Component({selector: 'app-document-table', template: ''})
class DocumentTableStubComponent {
    @Input() docs;
}

describe('MainComponent', () => {
    let component: MainComponent;
    let fixture: ComponentFixture<MainComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                MainComponent,
                SearchParamsStubComponent,
                DocumentTableStubComponent
            ],
            imports: [
                MaterialProxyModule,
                BrowserAnimationsModule,
                ReactiveFormsModule,
                HttpClientModule,
            ],
            providers: [
                { provide: MatDialog, useClass: MatDialogMock },
                { provide: DocumentService, useClass: DocumentServiceMock },
                { provide: SortingService, useClass: SortingServiceMock},
                {
                    provide: HAMMER_LOADER,
                    useValue: () => new Promise(() => {})
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MainComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should open dialog on add document', () => {
        component.openDocumentAdding('in');

        // @ts-ignore
        expect(component.dialog.count).toEqual(1);
    });

    it('should open dialog on open help', () => {
        component.openHelp();

        // @ts-ignore
        expect(component.dialog.count).toEqual(1);
    });

    it('should call search on search params', () => {
        component.onSearchParams({});

        // @ts-ignore
        expect(component.search.count).toEqual(1);
    });

    afterEach(() => {
        TestBed.resetTestingModule();
    });
});
