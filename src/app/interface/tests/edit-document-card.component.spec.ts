import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { EditDocumentCardComponent } from '../components/edit-document-card/edit-document-card.component';
import { MaterialProxyModule } from '../../material-proxy/material-proxy.module';
import { ReactiveFormsModule } from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DocumentService } from '../../data-access/services/document.service';
import { DataBuilder } from '../../shared/mocks/data-builder';
import * as moment from 'moment';
import { PDocument } from '../../shared/models/document';
import { PFile } from '../../shared/models/file';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

const currentDate = moment(Date.now());

class MockDialogRef {
    count = 0;

    close(data: any) {
        this.count++;
    }
}

class DocumentServiceMock {
    docs = DataBuilder.fakeDocuments(moment(Date.now()), 10);

    get fullDocuments() {
        return this.docs;
    }

    async editDocument(id: any, doc: any) {
        return 0;
    }

    async getDocument(id: number) {
        return DataBuilder.fakeDocument(currentDate);
    }
}

describe('EditDocumentCardComponent', () => {
    let component: EditDocumentCardComponent;
    let fixture: ComponentFixture<EditDocumentCardComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ EditDocumentCardComponent ],
            imports: [
                MaterialProxyModule,
                BrowserAnimationsModule,
                ReactiveFormsModule,
                HttpClientModule,
            ],
            providers: [
                { provide: MatDialogRef, useClass: MockDialogRef },
                { provide: MAT_DIALOG_DATA, useValue: { } },
                { provide: DocumentService, useClass: DocumentServiceMock }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EditDocumentCardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should close dialog on dialog event', () => {
        component.onClose();

        // @ts-ignore
        expect(component.dialogRef.count).toEqual(1);
    });

    it('should close dialog on edit document', fakeAsync(() => {
        const res = DataBuilder.fakeDocument(currentDate);
        component.editDocForm.setValue({
            description: res.description,
            correspondent: res.correspondent,
            otherCorrespondent: res.otherCorrespondent,
            worker: res.worker,
            clearFiles: false,
            files: null
        });

        tick(300);

        component.processEditDocument();

        tick(300);

        // @ts-ignore
        expect(component.dialogRef.count).toEqual(1);
    }));

    afterEach(() => {
        TestBed.resetTestingModule();
    });
});
