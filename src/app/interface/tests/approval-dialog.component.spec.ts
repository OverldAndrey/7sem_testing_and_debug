import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovalDialogComponent } from '../components/approval-dialog/approval-dialog.component';
import { MaterialProxyModule } from '../../material-proxy/material-proxy.module';
import { MatDialogRef } from '@angular/material/dialog';

class MockDialogRef { }

describe('ApprovalDialogComponent', () => {
    let component: ApprovalDialogComponent;
    let fixture: ComponentFixture<ApprovalDialogComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ApprovalDialogComponent ],
            imports: [ MaterialProxyModule ],
            providers: [
                { provide: MatDialogRef, useClass: MockDialogRef },
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ApprovalDialogComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    afterEach(() => {
        TestBed.resetTestingModule();
    });
});
