import { SortingService } from '../app/logic/services/sorting/sorting.service';
import {SearchParamsComponent} from '../app/interface/components/search-params/search-params.component';
import {DataBuilder, SearchParamsBuilder} from '../app/shared/mocks/data-builder';
import {PDocument} from '../app/shared/models/document';
import * as moment from 'moment';


let sortingService: SortingService;
let searchParamsComponent: SearchParamsComponent;
let paramsBuilder: SearchParamsBuilder;

const currentDate = moment(Date.now());

describe('Business Logic', () => {
    beforeAll(() => {
        sortingService = new SortingService();
        paramsBuilder = new SearchParamsBuilder();

        searchParamsComponent = new SearchParamsComponent();
    });

    it('should sort ascending by string id on form change', async () => {
        const documents = DataBuilder.fakeDocuments(currentDate, 10);
        const params = paramsBuilder
            .setSortOrder('asc')
            .setSort('stringIdSort')
            .setType('all')
            .setQuery('')
            .setTimesToNow()
            .extract();
        const resDocuments = documents.sort((a: PDocument, b: PDocument) =>
            a.stringId.localeCompare(b.stringId));

        searchParamsComponent.docSearch.setValue({
            query: params.query,
            type: params.type,
            fromCreateTime: params.createDateFrom,
            toCreateTime: params.createDateTo,
            sortOrder: params.sortOrder,
            sort: params.sort
        });
        searchParamsComponent.paramsChange.subscribe(async () => {
            const sorted = await sortingService.filterDocument(documents, params);
            expect(sorted).toEqual(resDocuments);
        });
    });

    it('should filter by substring on form change', async () => {
        const documents = DataBuilder.fakeDocuments(currentDate, 10);
        const params = paramsBuilder
            .setSortOrder('desc')
            .setSort('stringIdSort')
            .setType('all')
            .setQuery('ПСТ')
            .setTimesToNow()
            .extract();
        const resDocuments = documents
            .filter(d => d.stringId.toLowerCase().includes(params.query.toLowerCase()))
            .sort((a: PDocument, b: PDocument) => -a.stringId.localeCompare(b.stringId));

        searchParamsComponent.docSearch.setValue({
            query: params.query,
            type: params.type,
            fromCreateTime: params.createDateFrom,
            toCreateTime: params.createDateTo,
            sortOrder: params.sortOrder,
            sort: params.sort
        });
        searchParamsComponent.paramsChange.subscribe(async () => {
            const sorted = await sortingService.filterDocument(documents, params);
            expect(sorted).toEqual(resDocuments);
        });
    });
});
