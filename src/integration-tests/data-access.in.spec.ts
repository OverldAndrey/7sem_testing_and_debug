import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {TestBed, inject, fakeAsync, tick} from '@angular/core/testing';
import { ApiService } from '../app/data-access/services/api.service';
import { DataBuilder } from '../app/shared/mocks/data-builder';
import { environment } from '../environments/environment';
import { ProjectService } from '../app/data-access/services/project.service';
import { WorkerService } from '../app/data-access/services/worker.service';
import { Worker } from '../app/shared/models/worker';
import { CorrespondentService } from '../app/data-access/services/correspondent.service';
import { CaseService } from '../app/data-access/services/case.service';
import * as moment from 'moment';
import {DocumentService} from '../app/data-access/services/document.service';

let httpClient: HttpClient;
let httpTestingController: HttpTestingController;
let apiService: ApiService;

describe('Data Access', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule]
        });

        httpClient = TestBed.get(HttpClient);
        httpTestingController = TestBed.get(HttpTestingController);

        apiService = new ApiService(httpClient);
    });

    it('should fetch projects from server', () => {
        const testRes = DataBuilder.fakeProjects(10);
        const projectService = new ProjectService(apiService);

        projectService.init().then(() => {
            expect(projectService.fullProjects)
                .toEqual(testRes.map(p => ({id: p.id, name: p.name, groupName: p.projgroups.name, isActive: p.isActive})));
        });

        const req = httpTestingController.expectOne(environment.databaseURL + '/projects?select=id,name,isActive,projgroups(name)');

        req.flush(testRes);
    });

    it('should fetch workers from server', () => {
        const testRes = DataBuilder.fakeWorkers(10);
        const workerService = new WorkerService(apiService);

        workerService.init().then(() => {
            expect(workerService.fullWorkers).toEqual(testRes
                .map(wr => ({...wr, firstName: wr.firstname, secondName: wr.secondname, position: wr.positions.name}))
                .sort((a: Worker, b: Worker) =>
                    (a.secondName + ' ' + a.firstName).localeCompare(b.secondName + ' ' + b.firstName)));
        });

        const req = httpTestingController
            .expectOne(environment.databaseURL + '/workers?select=id,firstname,secondname,email,phone,isActive,positions(name)');

        req.flush(testRes);
    });

    it('should fetch correspondents from server', () => {
        const testRes = DataBuilder.fakeCorrespondents(10);
        const correspondentService = new CorrespondentService(apiService);

        correspondentService.init().then(() => {
            expect(correspondentService.correspondents).toEqual(testRes);
        });

        const req = httpTestingController
            .expectOne(environment.databaseURL + '/correspondents');

        req.flush(testRes);
    });

    it('should fetch cases from server', () => {
        const testRes = DataBuilder.fakeCases(10);
        const caseService = new CaseService(apiService);

        caseService.init().then(() => {
            expect(caseService.cases).toEqual(testRes);
        });

        const req = httpTestingController
            .expectOne(environment.databaseURL + '/cases');

        req.flush(testRes);
    });

    it('should fetch documents from server', () => {
        const currentDate = moment(Date.now());
        const testRes = DataBuilder.fakeDocuments(currentDate, 10);
        const documentService = new DocumentService(apiService);

        documentService.init().then(() => {
            expect(documentService.fullDocuments).toEqual(testRes);
        });

        const req = httpTestingController.expectOne(environment.databaseURL + '/rpc/getfulldocuments');

        req.flush(testRes);
    });

    it('should post document and update own documents', fakeAsync(() => {
        const currentDate = moment(Date.now());
        const testRes = DataBuilder.fakeDocuments(currentDate, 10);
        const insDoc = DataBuilder.fakeDocument(currentDate);
        const documentService = new DocumentService(apiService);

        documentService.init();

        const req = httpTestingController.expectOne(environment.databaseURL + '/rpc/getfulldocuments');

        req.flush(testRes);

        tick(100);

        documentService.addDocument(insDoc, '', []).then(() => {
            expect(documentService.fullDocuments).toContain(insDoc);
        });

        const req1 = httpTestingController.expectOne(environment.databaseURL + '/rpc/adddocument');

        req1.flush(insDoc.stringId);

        tick();

        const req2 = httpTestingController.expectOne(environment.databaseURL + '/rpc/getfulldocuments');

        req2.flush(testRes.concat(insDoc));
    }));

    afterEach(() => {
        httpTestingController.verify();
    });
});
