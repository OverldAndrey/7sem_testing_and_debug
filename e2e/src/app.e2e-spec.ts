import { AppPage } from './app.po';

import * as faker from 'faker';

let page: AppPage;
const wait = 1000;
let description;

describe('Docloc e2e document adding', () => {

    beforeAll(() => {
        debugger;
    });

    beforeEach(async () => {
        description = faker.lorem.paragraph(2);

        page = new AppPage();

        await page.navigateTo();
    });

    it('should add document to table and display it', async () => {
        // page.navigateTo();
        // expect(page.getTitleText()).toEqual('dbcourse app is running!');
        expect(await page.getTitleText()).toContain('Система внутреннего документооборота');

        expect(await page.getAddDocumentDialogPresence())
            .toBeFalsy('Add document dialog should not be present before click');

        await page.getAddDocumentButton().click();

        expect(await page.getAddDocumentDialogPresence())
            .toBeTruthy('Add document dialog should be present after click');

        const cors = page.getCorrespondentField();
        await cors.click();
        await page.sleep(wait);
        await page.getMatSelectWithValue(2).click();

        const wrks = page.getWorkerField();
        await wrks.click();
        await page.sleep(wait);
        await page.getMatSelectWithValue(2).click();

        const projs = page.getProjectField();
        await projs.click();
        await page.sleep(wait);
        await page.getMatSelectWithValue(2).click();

        const cases = page.getCaseField();
        await cases.click();
        await page.sleep(wait);
        await page.getMatSelectWithValue(2).click();

        await page.getDescriptionField().sendKeys(description);

        await page.getCommitDocumentButton().click();

        await page.sleep(wait);

        expect(await page.getAddDocumentDialogPresence())
            .toBeFalsy('Add document dialog should close on document add');

        await page.sleep(wait);

        expect(await page.getDocumentTableText()).toContain(description);

        await page.getDocumentTableFirstRow().click();

        expect(await page.getDocumentDescriptionDialogPresence()).toBeTruthy();
        expect(await page.getDocumentDescriptionDialogTableText()).toContain(description);
    });

    afterEach(async () => {
        // Assert that there are no errors emitted from the browser
        // const logs = await browser.manage().logs().get(logging.Type.BROWSER);
        // expect(logs).not.toContain(jasmine.objectContaining({
        //     level: logging.Level.SEVERE,
        // } as logging.Entry));
    });

    afterAll(() => {
        debugger;
    });
});
