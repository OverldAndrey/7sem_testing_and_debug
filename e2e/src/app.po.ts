import { browser, by, element } from 'protractor';

export class AppPage {
    navigateTo() {
        return browser.get('/') as Promise<any>;
    }

    getTitleText() {
        return element(by.className('app-toolbar')).getText() as Promise<string>;
    }

    getAddDocumentDialogPresence() {
        return element(by.tagName('app-add-document-card')).isPresent() as Promise<boolean>;
    }

    getAddDocumentButton() {
        return element(by.id('add-document-btn'));
    }

    getCorrespondentField() {
        return element(by.id('correspondent'));
    }

    getWorkerField() {
        return element(by.id('worker'));
    }

    getProjectField() {
        return element(by.id('project'));
    }

    getCaseField() {
        return element(by.id('case'));
    }

    getDescriptionField() {
        return element(by.id('description'));
    }

    getMatSelectWithValue(n: number) {
        return element(by.css(`.mat-option[ng-reflect-value="${n}"]`));
    }

    sleep(ms: number) {
        return browser.sleep(ms) as Promise<void>;
    }

    getCommitDocumentButton() {
        return element(by.id('add-document-commit-btn'));
    }

    getDocumentTableFirstRow() {
        return element.all(by.className('doc-table-main')).first().element(by.tagName('tbody')).all(by.tagName('tr')).first();
    }

    getDocumentTableText() {
        return element.all(by.className('doc-table-main')).first().element(by.tagName('tbody'))
            .all(by.tagName('tr')).first().getText() as Promise<string>;
    }

    getDocumentDescriptionDialogPresence() {
        return element(by.tagName('app-document-details')).isPresent() as Promise<boolean>;
    }

    getDocumentDescriptionDialogTableText() {
        return element(by.tagName('app-document-details')).element(by.tagName('tbody')).getText() as Promise<string>;
    }
}
