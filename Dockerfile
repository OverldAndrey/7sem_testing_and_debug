FROM node:12.18.2-alpine as build
COPY . ./docloc
WORKDIR /docloc
RUN npm i
RUN sed -i "s|address|${HTTP_PROXY}|g" ./src/environments/environment.prod.ts
RUN cat ./src/environments/environment.prod.ts
RUN npm run build -- --prod

FROM nginx:alpine
COPY --from=build /docloc/dist/dbcourse/ /usr/share/nginx/html
