# -------------------------------------- Sending docker_stack.yml --------------------------------------------------

if sshpass -p $DEPLOY_HOST_USER_PASSWORD scp -o StrictHostKeyChecking=no docker-compose.yml $DEPLOY_HOST_USER@$DEPLOY_HOST_ADDRESS:~/$DEPLOY_FOLDER;
then
	echo "[INFO] docker_compose.yml has been sent."
else
	echo "[ERROR] Could not send docker_compose.yml."
	exit 1
fi

if sshpass -p $DEPLOY_HOST_USER_PASSWORD scp -o StrictHostKeyChecking=no -r docker-entrypoint-initdb.d $DEPLOY_HOST_USER@$DEPLOY_HOST_ADDRESS:~/$DEPLOY_FOLDER;
then
	echo "[INFO] docker-entrypoint-initdb.d has been sent."
else
	echo "[ERROR] Could not send docker-entrypoint-initdb.d."
#	exit 1
fi

# --------------------------------- Run deploy.sh on remote server ----------------------------------------------------

if sshpass -p $DEPLOY_HOST_USER_PASSWORD ssh -o StrictHostKeyChecking=no $DEPLOY_HOST_USER@$DEPLOY_HOST_ADDRESS bash -s < deploy/deploy.sh $DEPLOY_FOLDER;
then
	echo "[INFO] the deploy.sh script was executed."
	echo OK
else
	echo "[ERROR] error on executing deploy.sh script!"
	exit 2
fi
