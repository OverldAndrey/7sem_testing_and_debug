DEPLOY_FOLDER=$1

cd $DEPLOY_FOLDER

docker-compose pull
docker-compose down
docker-compose up -d
