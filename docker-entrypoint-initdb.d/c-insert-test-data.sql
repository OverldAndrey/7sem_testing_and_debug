insert into dbcourse.projGroups
values (1, 'Group 1'),
       (2, 'Group 2'),
       (3, 'Group 3');

insert into dbcourse.projects
values (1, 'qerwr werwere', 2, true),
       (2, 'klmkcv lmlsd', 3, true),
       (3, 'iniejin dfjjdfnjwfnwnfj', 2, false),
       (4, 'mdsfm', 1, true),
       (5, 'jwenjfjfnnfwejwnken wenfjkwnkfn', 1, true);

insert into dbcourse.cases
values (1, 'oiwfjie ojfjewfkn lnfd', 'wnfjnwenjfn wefjnkwn'),
       (2, 'okfwkmwef', 'weifwejf, wekjfnjknwejjf'),
       (3, 'wfkikfwekl', 'ijsdnjdnf');

insert into dbcourse.positions
values (1, 'Coder'),
       (2, 'Lead'),
       (3, 'Intern');

insert into dbcourse.workers
values (1, 'Fwkwekfe', 'Fwowmkasko', 'wfwd@ffw.com', '5231537256', true, 1),
       (2, 'Vmsal', 'Qlfdl', 'df@dsdlm.com', '2998213899', true, 3),
       (3, 'Mmewfo', 'Cokqqkod', 'wfefe@wfmw.ru', '1392983781', true, 3),
       (4, 'Dkdkekp', 'Lewofejwf', 'fdsdf@kdfo.com', '3903903939', false, 2),
       (5, 'Pjode', 'Cjkedek', 'efefkdls@dnsofnf.com', '1948178893', true, 1);

insert into dbcourse.correspondents
values (1, 'fdcwdfwfww', 'whife@dojfw.net', '939849384829'),
       (2, 'fgeerk', 'rfjirfr@oejf.com', '1874315577');

insert into dbcourse.documents("stringId", "parentStringId", correspondent, "otherCorrespondent",
    "case", project, worker, description, "createDate")
values ('ПСТ-001', null, 2, '', 1, 2, 1, 'gsdgcfsghcwghxfdhnjxdskhnf', now()),
       ('ПСТ-002', 'ПСТ-001', 2, '', 1, 2, 3, 'whfwhfuhdmjfmjsmkjf', now()),
       ('Вх.ПС-001', null, 1, '', 3, 5, 2, 'wffjifnwun28nxuhnudfh', now()),
       ('Вх.ПС-002', null, 2, '', 1, 2, 4, 'erjenjenjienenjknejlkfnjkfnjr', now());

insert into dbcourse.documentRefs
values (2, 1);

insert into dbcourse.files(document, name, type, data)
values (1, 'rojefkm.txt', 'text/plain', 'abc'::bytea),
       (1, 'dlkfm.txt', 'text/plain', 'abc'::bytea),
       (3, 'ojrefkr.txt', 'text/plain', 'abc'::bytea),
       (4, 'lekeflefllflmf.txt', 'text/plain', 'abc'::bytea),
       (4, 'ljijreifbdf.txt', 'text/plain', 'abc'::bytea);
