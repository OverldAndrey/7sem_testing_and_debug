const pgp = require("pg-promise")(/*options*/);
const db = pgp("postgres://postgres:postgres@postgres:5432/postgres");


async function main() {
    await db.query((new pgp.QueryFile('./a-create-tables.sql')));
    await db.query((new pgp.QueryFile('./b-func.sql')));
    await db.query((new pgp.QueryFile('./c-insert-test-data.sql')));
}

main().catch(console.log);
