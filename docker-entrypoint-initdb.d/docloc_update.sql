drop function dbcourse.adddocument(document text, docrefs text, files text);

create or replace function dbcourse.addDocument(
    document text,
    docrefs text,
    files text)
    returns varchar
    language plpgsql
as $$
declare
    mid bigint;
    strid varchar;
    nofstrids bigint;
begin
    insert into dbcourse.documents("stringId", "parentStringId", correspondent, "otherCorrespondent",
                                   "case", project, worker, description, "createDate") select *
    from (select * from json_to_recordset(document::json)
                            as f("stringId" varchar, "parentStringId" varchar, correspondent bigint, "otherCorrespondent" varchar,
                                 "case" bigint, project bigint, worker bigint, description text, "createDate" timestamp with time zone)) as nd;

    select max(id) into mid from dbcourse.documents;
    select "stringId" into strid from dbcourse.documents where id=mid;
    select count(*) into nofstrids from dbcourse.documents where "stringId"=strid and extract(year from "createDate")=2020;

    if nofstrids > 1 then
        raise exception using
            message = 'Violating constraint "stringidunique"',
            errcode = '23505';
    end if;

    if docrefs != '[]' then
        insert into dbcourse.documentrefs(primedoc, refdoc) select mid as primeDoc, rd.refdoc as refdoc
        from (select * from json_to_recordset(docrefs::json) as f(primedoc bigint, refdoc bigint))
                 as rd(primedoc, refdoc);
    end if;

    insert into dbcourse.files(document, name, type, data) select mid as document, fs.name as name, fs.type as type, fs.data as data
    from (select * from json_to_recordset(files::json)
                            as f(document bigint, name varchar, type varchar, data bytea))
             as fs(document, name, type, data);
    return strid;
end
$$;
