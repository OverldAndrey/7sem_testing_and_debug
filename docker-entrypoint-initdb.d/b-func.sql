create function dbcourse.getDocRefs(docid text)
returns table(id bigint,
    "stringId" varchar(128),
    "parentStringId" varchar(128),
    correspondent bigint,
    "otherCorrespondent" varchar(256),
    "case" bigint,
    project bigint,
    worker bigint,
    description text,
    "createDate" timestamp with time zone
             )
immutable
language sql
as
$$
    select
    id,
    "stringId",
    "parentStringId",
    correspondent,
    "otherCorrespondent",
    "case",
    project,
    worker,
    description,
    "createDate"
    from dbcourse.documentRefs join dbcourse.documents on refDoc=id where primeDoc=cast(docid as bigint);
$$;

create or replace function dbcourse.getFile(fid text)
returns setof dbcourse.files
language plpgsql
as $$
-- declare
--     ba setof dbcourse.files;
begin
    return query select * from dbcourse.files where id=cast(fid as bigint);
end
$$;

create function dbcourse.getFullDocuments()
returns table(id bigint,
    "stringId" varchar(128),
    "parentStringId" varchar(128),
    correspondent varchar,
    "otherCorrespondent" varchar(256),
    "case" varchar,
    project varchar,
    worker varchar,
    description text,
    "createDate" timestamp with time zone
             )
immutable
language sql
as $$
    select
    documents.id,
    "stringId",
    "parentStringId",
    concat(c2.name, ' <', c2.email, '>') as correspondent,
    "otherCorrespondent",
    c.name as "case",
    concat(p.name, ', ', p3.name) as project,
    concat(w.firstname, ' ', w.secondname, ', ', p2.name, ' <', w.email, '>') as worker,
    description,
    "createDate"
    from dbcourse.documents
        join dbcourse.cases c on dbcourse.documents."case" = c.id
        join dbcourse.correspondents c2 on dbcourse.documents.correspondent = c2.id
        join dbcourse.projects p on dbcourse.documents.project = p.id
        join dbcourse.workers w on dbcourse.documents.worker = w.id
        join dbcourse.positions p2 on w.position = p2.id
        join dbcourse.projgroups p3 on p."group" = p3.id;
$$;

create or replace function dbcourse.addDocument(
    document text,
    docrefs text,
    files text)
    returns varchar
    language plpgsql
as $$
declare
    mid bigint;
    strid varchar;
    nofstrids bigint;
begin
    insert into dbcourse.documents("stringId", "parentStringId", correspondent, "otherCorrespondent",
                                   "case", project, worker, description, "createDate") select *
    from (select * from json_to_recordset(document::json)
                            as f("stringId" varchar, "parentStringId" varchar, correspondent bigint, "otherCorrespondent" varchar,
                                 "case" bigint, project bigint, worker bigint, description text, "createDate" timestamp with time zone)) as nd;

    select max(id) into mid from dbcourse.documents;
    select "stringId" into strid from dbcourse.documents where id=mid;
    select count(*) into nofstrids from dbcourse.documents where "stringId"=strid and extract(year from "createDate")=2020;

    if nofstrids > 1 then
        raise exception using
            message = 'Violating constraint "stringidunique"',
            errcode = '23505';
    end if;

    if docrefs != '[]' then
        insert into dbcourse.documentrefs(primedoc, refdoc) select mid as primeDoc, rd.refdoc as refdoc
        from (select * from json_to_recordset(docrefs::json) as f(primedoc bigint, refdoc bigint))
                 as rd(primedoc, refdoc);
    end if;

    insert into dbcourse.files(document, name, type, data) select mid as document, fs.name as name, fs.type as type, fs.data as data
    from (select * from json_to_recordset(files::json)
                            as f(document bigint, name varchar, type varchar, data bytea))
             as fs(document, name, type, data);
    return strid;
end
$$;

create or replace function dbcourse.replaceFiles(docId text, files text)
returns void
language plpgsql
as $$
begin
    delete from dbcourse.files where document=cast(docId as bigint);

    insert into dbcourse.files(document, name, type, data)
        select fs.document as document, fs.name as name, fs.type as type, fs.data as data
        from (select * from json_to_recordset(files::json)
            as f(document bigint, name varchar, type varchar, data bytea))
            as fs(document, name, type, data);
end;
$$;

-- select * from dbcourse.addDocument('kek'::text, '[{"primedoc": 0, "refdoc": 3}]'::text, 'qwerty'::text);
-- select * from dbcourse.getfile(fid := '1');
-- select * from json_to_recordset(to_json('{"primedoc": 0, "refdoc": 3}'::text)::json) as f(primedoc bigint, refdoc bigint);
-- select * from json_to_recordset('[{"primedoc": 0, "refdoc": 3}]'::json) as f(primedoc bigint, refdoc bigint);

